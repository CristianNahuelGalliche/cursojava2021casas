package modulo4;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner ingresar = new Scanner(System.in);
			
			System.out.println("Ingresar un numero :");
			int numero =ingresar.nextInt();
			
			if (numero> 0 && numero< 13) {
				System.out.println("El numero " + numero + "entra en la primera docena" );
			}
			else if (numero > 12 && numero < 25) {
				System.out.println("El numero " + numero + "entra en la segunda docena" );
			}
			else if (numero > 24 && numero < 37) {
				System.out.println("El numero " + numero + "entra en la tercera docena" );
			}
			else {
				System.out.println("El numero " + numero + "no entra en las docenas" );
			}
	ingresar.close();
	}

}
