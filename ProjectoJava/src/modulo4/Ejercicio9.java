package modulo4;

import java.util.Scanner;


public class Ejercicio9 {

	public static void main(String[] args) {
		Scanner ingresar = new Scanner(System.in);
		
		System.out.println("Jugador 1 ingresar su jugada (0) piedra, (1) papel, (2) tijera : ");
		int player1= ingresar.nextInt();
		
		System.out.println("Jugador 2 ingresar su jugada (0) piedra, (1) papel, (2) tijera : ");
		int player2= ingresar.nextInt();

		if(player1<0 || player1>2 ||player2<0 || player2>2){
			System.out.println("ERROR: Jugada inexistente,realize otra jugada");
		}
		else if (player1 == player2) {
			System.out.println("Los dos jugadores eligieron lo mismo ");
		}
		else {
					if (player1 == 0 && player2 == 1) {
						System.out.println("Gana el jugador 2, el papel le gana a la piedra");
					}
					if (player1 == 0 && player2 == 2) {
						System.out.println("Gana el jugador 1, la tijera la gana al papel");
						
					}
					if (player1 == 1 && player2 == 0) {
						System.out.println("Gana el jugador 1, el papel la gana a la piedra");
					}
					if (player1 == 1 && player2 == 2) {
						System.out.println("Gana el jugador 2, la tijera la gana al papel ");
						
					}
					if (player1 == 2 && player2 ==0  ) {
						System.out.println("Gana el jugador 2, la piedra le gana a la tijera ");
						
					}
					if (player1 == 2 && player2 == 1) {
						System.out.println("Gana el jugador 1,el papel le gana a la piedra ");
						
					}	
			}
	ingresar.close();	
	}

}
