package modulo4;

public class ejercicio18 {

	public static void main(String[] args) {
System.out.println("Las tablas de multiplicar del 0 al 10:");
		
		for(int numero=0;numero<101;numero++){
			System.out.println("\nLa tabla del " + numero + " es:");
			for(int i=0;i<11;i++){
				System.out.println(numero + " x " + i + " = " + numero*i);
			}
		}

	}

}
