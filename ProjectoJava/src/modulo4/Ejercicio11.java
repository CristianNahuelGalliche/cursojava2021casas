package modulo4;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner ingresar = new Scanner(System.in);
		
		System.out.println("Ingresar un caracter :");
		char caracter=ingresar.next().charAt(0);
		
		if (caracter == 'a' || caracter == 'A' || caracter == 'e' || caracter == 'E' || caracter == 'i' || caracter == 'I' 
				|| caracter == 'o'|| caracter == 'O' || caracter == 'u' || caracter =='U') {
			System.out.println("El caracter " + caracter + " es una vocal");
		}
		else {
			System.out.println("El caracter " + caracter + " no es una vocal");
		}
	ingresar.close();
	}

}
