package modulo4;

public class ejercicio20 {

	public static void main(String[] args) {
		System.out.println("Los numeros al azar son: ");
		int numeros;
		int min=0;
		int max=0;
		int a=1;
		
		do { 
			numeros = (int)(Math.random()*1000);
			if(a==1) {
					min=numeros;
					max=numeros;
			}
			else if (numeros>max) {
				max=numeros;
			}
			else if (numeros<min) {
				min=numeros;
			}
			System.out.println(numeros);
			a++;
		} while (a<11);
		System.out.println("\nEl numero maximo es: "+ max);
		System.out.println("\nEl numero minimo es: "+ min);
	}

}
