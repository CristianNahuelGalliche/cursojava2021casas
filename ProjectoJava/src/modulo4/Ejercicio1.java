package modulo4;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		Scanner ingresar = new Scanner(System.in);
		
		System.out.println("Ingresar la primer nota del trimestre: ");
		float primertrimestre = ingresar.nextFloat();
		
		System.out.println("Ingresar la segunda nota del trimestre: ");
		float segundotrimestre = ingresar.nextFloat();

		System.out.println("Ingresar la tercera nota del trimestre: ");
		float tercertrimestre = ingresar.nextFloat();
		
		float promedio = (primertrimestre + segundotrimestre + tercertrimestre)/3;
		
	if(promedio>=7) {
		System.out.println("Aprobado/a con un promedio de " + promedio);
	}
	
	else {
		System.out.println("Reprodado/a con un promedio de " + promedio);
	}
	ingresar.close();
	}

}
