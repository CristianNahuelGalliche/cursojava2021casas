package modulo4;

import java.util.Scanner;

public class ejercicio15 {

	public static void main(String[] args) {
Scanner ingresar = new Scanner(System.in);
		
		System.out.println("Ingresar un tipo de auto a b o c :");
		char categoria=ingresar.next().charAt(0);
	
		switch(categoria){
		 case 'a':
			 System.out.println("El auto cuenta con cuatro ruedas y un motor.");
			 break;
		 case 'b':
			 System.out.println("El auto cuenta con cuatro ruedas, un motor, cierre centralizado y aire.");
			 break;
		 case 'c':
			 System.out.println("El auto cuenta con cuatro ruedas, un motor, cierre centralizado, aire y airbag.");
			 break;
		 default:
				System.out.println("No existe esta categoria vuelva a intentarl.");
		}
	
	ingresar.close();
	
	}

}
