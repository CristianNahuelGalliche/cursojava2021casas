package modulo2;

public class Ejercicio5 {

	public static void main(String[] args) {
		byte b=0;
		short s=1;
		int i=2;
		long l=3;
		
		i=s; //Aqui no hay errores.
		
		//b=s; Existe un error de compilacion y se debe corregir as�:
		b=(byte)s;
		
		
		l=i; //Aqui no hay errores
		
		//b=i;Existe un error de compilacion y se debe corregir as�:
		b=(byte)i;
		
		
		//s=i;Existe un error de compilacion y se debe corregir as�:
		
		s=(short)i;
		
		System.out.println(b + "\n" + s + "\n" + i + "\n" + l);


	}

}
